<?php

namespace Kanban\Component;

use Kanban\Component\EventListener\ExceptionHandler;

use Kanban\Component\EventListener\RequestListener;
use Kanban\Component\EventListener\ResponseListener;
use Kanban\Component\EventListener\StringToResponseListener;

use Kanban\Services\UserProvider;

use Silex;
use Silex\Application;

abstract class Kernel extends Application
{

    public function __construct(array $values = array())
    {
        parent::__construct($values);

        $app = $this;

        $this->addConfig($app);
        $this->registerServices($app);
        $this->addSecurity($app);
        $this->addSubscribers($app);
        $this->registerControllers($app);
    }

    abstract protected function registerControllers($app);
    abstract protected function registerServices($app);

    /**
     * @param Application $app
     */
    protected function addConfig($app)
    {
        $config = require_once __DIR__ . '/../../../app/config.php';

        foreach ($config as $service => $options) {
            $app[$service] = $options;
        }
    }

    /**
     * @param $app
     */
    protected function addSubscribers($app)
    {
        $app['dispatcher']->addSubscriber(new RequestListener($app));
        $app['dispatcher']->addSubscriber(new ResponseListener($app));
        $app['dispatcher']->addSubscriber(new StringToResponseListener($app));
        $app['dispatcher']->addSubscriber(new ExceptionHandler($app));
    }


    /**
     * @param Application $app
     */
    protected function addSecurity($app)
    {
        $app['users'] = function () use ($app) {
            return new UserProvider($app['redis'], $app['security.encoder.digest']);
        };

        $app['security.firewalls'] = array(
            'login' => [
                'pattern' => 'login|register|oauth',
                'anonymous' => true,
            ],
            'secured' => array(
                'pattern' => '^.*$',
                'logout' => array('logout_path' => '/logout'),
                'users' => $app['users'],
                'jwt' => array(
                    'use_forward' => true,
                    'require_previous_session' => false,
                    'stateless' => true,
                )
            ),
        );
    }
}
