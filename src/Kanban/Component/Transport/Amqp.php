<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 09/01/15
 * Time: 19:41
 */

namespace Kanban\Component\Transport;

use \AMQPConnection;
use \AMQPChannel;
use \AMQPExchange;
use \AMQPQueue;
use \AMQPEnvelope;
use \Exception;

class Amqp {
    /**
     * ключи конфигурации
     */
    const CONFIG_CONNECTION                  = "connection";
    const CONFIG_RESPONSE_QUEUES             = "responseQueues";
    const CONFIG_EXCHANGES                   = "exchanges";
    const CONFIG_PUBLISHERS_ARGUMENTS        = "publishersArguments";
    const CONFIG_DEFAULT_PUBLISHER_ARGUMENTS = "defaultPublisherArguments";
    const CONFIG_ROUTING_KEYS                = "routingKeys";

    /**
     * @var AMQPConnection
     */
    protected $connection;

    /**
     * @var AMQPChannel
     */
    protected $channel;

    /**
     * @var array
     */
    protected $exchanges;

    /**
     * @var array
     */
    protected $exchangesOptions;

    /**
     * @var array
     */
    protected $responseQueues;

    /**
     * @var array
     */
    protected $responseQueuesOptions;

    /**
     * @var array
     */
    protected $routingKeys;

    /**
     * @var array
     */
    protected $publisherArgumentsOptions;

    /**
     * @var array
     */
    protected $publisherArguments;

    /**
     * @var array
     */
    protected $defaultPublisherArguments = array(
        "delivery_mode"    => 1,
        "content_type"     => "text/plain",
        "content_encoding" => "base64",
    );

    /**
     * @var array
     */
    protected $connectionConfig;

    /**
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->setConfig($config);
    }

    /**
     * @param array $config
     *
     * @throws \Exception
     */
    protected function setConfig(array $config)
    {
        if (!array_key_exists(self::CONFIG_CONNECTION, $config)) {
            throw new Exception("Configuration must have a key '" . self::CONFIG_CONNECTION . "'");
        }

        if (!is_array($config[self::CONFIG_CONNECTION])) {
            throw new Exception("Parameter '" . self::CONFIG_CONNECTION . "' must be an array");
        }

        $this->connectionConfig = $config[self::CONFIG_CONNECTION];

        if (array_key_exists(self::CONFIG_RESPONSE_QUEUES, $config)) {
            if (!is_array($config[self::CONFIG_RESPONSE_QUEUES])) {
                throw new Exception("Parameter '" . self::CONFIG_RESPONSE_QUEUES . "' must be an array");
            }

            foreach ($config[self::CONFIG_RESPONSE_QUEUES] as $serviceName => $queueConfig) {
                $this->setResponseQueueForService($serviceName, $queueConfig);
            }
        }

        if (array_key_exists(self::CONFIG_EXCHANGES, $config)) {
            if (!is_array($config[self::CONFIG_EXCHANGES])) {
                throw new Exception("Parameter '" . self::CONFIG_EXCHANGES . "' must be an array");
            }

            foreach ($config[self::CONFIG_EXCHANGES] as $serviceName => $exchangeConfig) {
                $this->setExchangeForService($serviceName, $exchangeConfig);
            }
        }

        if (array_key_exists(self::CONFIG_PUBLISHERS_ARGUMENTS, $config)) {
            if (!is_array($config[self::CONFIG_PUBLISHERS_ARGUMENTS])) {
                throw new Exception("Parameter '" . self::CONFIG_PUBLISHERS_ARGUMENTS . "' must be an array");
            }

            foreach ($config[self::CONFIG_PUBLISHERS_ARGUMENTS] as $serviceName => $publisherArguments) {
                $this->setPublisherArguments($serviceName, $publisherArguments);
            }
        }

        if (array_key_exists(self::CONFIG_DEFAULT_PUBLISHER_ARGUMENTS, $config)) {
            if (!is_array($config[self::CONFIG_DEFAULT_PUBLISHER_ARGUMENTS])) {
                throw new Exception("Parameter '" . self::CONFIG_DEFAULT_PUBLISHER_ARGUMENTS . "' must be an array");
            }

            $this->setDefaultPublisherArguments($config[self::CONFIG_DEFAULT_PUBLISHER_ARGUMENTS]);
        }

        if (array_key_exists(self::CONFIG_ROUTING_KEYS, $config)) {
            if (!is_array($config[self::CONFIG_ROUTING_KEYS])) {
                throw new Exception("Parameter '" . self::CONFIG_ROUTING_KEYS . "' must be an array");
            }

            foreach ($config[self::CONFIG_ROUTING_KEYS] as $serviceName => $routingKey) {
                $this->setRoutingKey($serviceName, $routingKey);
            }
        }
    }

    public function cast($serviceName, $routingKey, $args)
    {
        $exchange = $this->getExchange($serviceName);
        $publisherArguments = $this->getPublisherArguments($serviceName);

        return $exchange->publish($args, $routingKey, AMQP_MANDATORY, $publisherArguments);
    }

    /**
     * @return AMQPConnection
     */
    protected function getConnection()
    {
        if ($this->connection === null) {
            $this->connection = new AMQPConnection($this->connectionConfig);
            $this->connection->connect();
        }

        return $this->connection;
    }

    /**
     * @return AMQPChannel
     */
    protected function getChannel()
    {
        if ($this->channel === null) {
            $connection = $this->getConnection();
            $this->channel = new AMQPChannel($connection);
        }

        return $this->channel;
    }

    /**
     * @param string $serviceName
     *
     * @return array
     */
    public function getPublisherArguments($serviceName)
    {
        if (isset($this->publisherArguments[$serviceName])) {
            return $this->publisherArguments[$serviceName];
        }

        if (isset($this->publisherArgumentsOptions[$serviceName])) {
            $defaults = $this->defaultPublisherArguments;
            $this->publisherArguments[$serviceName] = array_merge($defaults, $this->publisherArgumentsOptions[$serviceName]);

            return $this->publisherArguments[$serviceName];
        }

        $this->publisherArguments[$serviceName] = $this->defaultPublisherArguments;

        return $this->publisherArguments[$serviceName];
    }

    /**
     * @param array $serviceName
     * @param array $arguments
     *
     * @return bool
     */
    public function setPublisherArguments($serviceName, array $arguments)
    {
        $arguments = array_intersect_key($arguments, $this->defaultPublisherArguments);

        $this->publisherArgumentsOptions[$serviceName] = $arguments;

        return true;
    }

    /**
     * @param array $arguments
     *
     * @return bool
     */
    public function setDefaultPublisherArguments(array $arguments)
    {
        $arguments = array_intersect_key($arguments, $this->defaultPublisherArguments);

        $this->defaultPublisherArguments = $arguments;

        return true;
    }

    /**
     * @return array
     */
    public function getDefaultPublisherArguments()
    {
        return $this->defaultPublisherArguments;
    }


    /**
     * @param string $serviceName
     *
     * @return AMQPExchange
     */
    protected function getExchange($serviceName)
    {
        if (isset($this->exchanges[$serviceName])) {
            return $this->exchanges[$serviceName];
        }

        if (isset($this->exchangesOptions[$serviceName])) {
            $this->exchanges[$serviceName] = $this->initExchange($serviceName);

            return $this->exchanges[$serviceName];
        }

        if (!isset($this->exchanges["default"])) {
            $this->exchanges["default"] = $this->initExchange("default");
        }

        $this->exchanges[$serviceName] = $this->exchanges["default"];

        return $this->exchanges[$serviceName];
    }

    /**
     * $queueConfig is an array containing options:
     * name      => (string) queue name
     * bindings  => (array) key - routingKey, value - exchange name, or list of exchanges
     * flags     => (int) integer bitmask of all the flags
     * arguments => (array) an array of key/value pairs of arguments
     *
     * @param string $serviceName
     * @param array  $queueConfig
     *
     * @return bool
     */
    public function setResponseQueueForService($serviceName, array $queueConfig)
    {
        $this->responseQueuesOptions[$serviceName] = array(
            "name"      => !empty($queueConfig["name"])      ? $queueConfig["name"]      : "",
            "bindings"  => !empty($queueConfig["bindings"])  ? $queueConfig["bindings"]  : array(),
            "flags"     => isset($queueConfig["flags"])      ? $queueConfig["flags"]     : AMQP_AUTODELETE,
            "arguments" => !empty($queueConfig["arguments"]) ? $queueConfig["arguments"] : array(),
        );

        return true;
    }

    /**
     * $exchangeConfig is an array containing options:
     * name      => (string) exchange name
     * type      => (string) exchange type
     * flags     => (int) integer bitmask of all the flags
     * arguments => (array) an array of key/value pairs of arguments
     *
     * @param string $serviceName
     * @param array  $exchangeConfig
     *
     * @return bool
     */
    public function setExchangeForService($serviceName, array $exchangeConfig)
    {
        if (!array_key_exists("name", $exchangeConfig)) {
            throw new Exception("Exchange configuration must contain the key 'name'");
        }

        if (!array_key_exists("type", $exchangeConfig)) {
            throw new Exception("Exchange configuration must contain the key 'type'");
        }

        $this->exchangesOptions[$serviceName] = array(
            "name"      => $exchangeConfig["name"],
            "type"      => $exchangeConfig["type"],
            "flags"     => isset($exchangeConfig["flags"])      ? $exchangeConfig["flags"]     : AMQP_NOPARAM,
            "arguments" => !empty($exchangeConfig["arguments"]) ? $exchangeConfig["arguments"] : array(),
        );

        return true;
    }

    /**
     * @param string $serviceName
     *
     * @return AMQPExchange
     */
    protected function initExchange($serviceName)
    {
        $options = $this->exchangesOptions[$serviceName];

        $exchange = new AMQPExchange($this->getChannel());
        $exchange->setName($options['name']);
        $exchange->setType($options['type']);
        $exchange->setFlags($options['flags']);
        $exchange->setArguments($options['arguments']);
        $exchange->declareExchange();

        return $exchange;
    }

    /**
     * @param string $serviceName
     *
     * @return string
     */
    public function getRoutingKey($serviceName)
    {
        if (isset($this->routingKeys[$serviceName])) {
            return $this->routingKeys[$serviceName];
        }

        return $serviceName;
    }

    /**
     * @param string $serviceName
     * @param string $routingKey
     */
    public function setRoutingKey($serviceName, $routingKey)
    {
        $this->routingKeys[$serviceName] = $routingKey;
    }
}