<?php

namespace Kanban\Controller;

use Symfony\Component\HttpFoundation\Request;

class BoardController
{

    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    /**
     * Get all projects for owned user
     *
     * @param Request $request
     *
     * @return array
     */
    public function getCollectionAction(Request $request)
    {
        $per_page = $request->query->get("per_page", 1000);

        $response = $this->app['gitlab_api']->executeCommand('GetProjects', 
        [
            'per_page' => $per_page, 
            'archived' => 'false'
        ]);

        return $response;
    }

    /**
     * Get one project
     *
     * @param Request $request
     *
     * @return array
     */
    public function getAction(Request $request)
    {
        $board_id = $request->query->get('project_id');

        $response = $this->app['gitlab_api']->executeCommand('GetProject', ['project_id' => $board_id]);

        return $response;
    }

    public function getEventsAction(Request $request)
    {
        $board_id = $request->query->get('project_id');

        $response = $this->app['gitlab_api']->executeCommand('GetProjectEvents', ['project_id' => $board_id]);

        return $response;
    }

    /**
     * Create labels for columns view
     *
     * @param Request $request
     *
     * @return array
     */
    public function postConfigureAction(Request $request)
    {
        $vars = $request->request->all();

        $project_id = $vars['project_id'];

        $labels = [
            'KB[stage][0][Backlog]',
            'KB[stage][1][Development]',
            'KB[stage][2][Testing]',
            'KB[stage][3][Production]',
            'KB[stage][4][Ready]',
        ];

        foreach ($labels as $label) {
            $this->app['gitlab_api']->executeCommand('CreateLabel', ['project_id' => $project_id, 'name' => $label, 'color' => '#F5F5F5']);
        }

        return $this->app->json(['success'=>true]);
    }
}
