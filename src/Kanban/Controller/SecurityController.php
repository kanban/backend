<?php

namespace Kanban\Controller;

use Gitlab\Api;
use Gitlab\Models\User;
use JWT;
use Kanban\Services\UserProvider;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;

class SecurityController
{

    /**
     * @var \Silex\Application
     */
    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function loginAction(Request $request)
    {
        $vars = $request->request->all();

        try {
            $user = $this->app['users']->loadUserByUsername($vars['_username']);
            if ($this->app['security.encoder.digest']->encodePassword($vars['_password'], '') !== $user->getPassword()) {
                throw new UsernameNotFoundException(sprintf('Username "%s" does not exist.', $vars['_username']));
            } else {
                $response = [
                    'success' => true,
                    'token' => $this->app['security.jwt.encoder']->encode(['name' => $user->getUsername()]),
                ];
            }
        } catch (UsernameNotFoundException $e) {
            $response = [
                'success' => false,
                'error' => 'Invalid credentials',
            ];
        }

        return $this->app->json($response, ($response['success'] == true ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST));
    }

    /**
     * Отправляет пользователя на нужный url, для oauth авторизации
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function oauthRedirectAction(Request $request)
    {
        $provider = $request->get('provider', null);

        if (null === $provider) {
            throw new BadRequestHttpException('Bad request params');
        }

       return $this->app->redirect($this->app['gitlab_api']->getAuthUrl());
    }

    /**
     * Отвечает
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @throws \Exception
     */
    public function oauthAction(Request $request)
    {
        $code = $request->request->get('code', null);
        $provider = $request->request->get('provider', 'gitlab');

        /**
         * @var $api Api
         */
        $api = $this->app['gitlab_api'];

        if (null === $code) {
            throw new BadRequestHttpException('Bad request params');
        }

        $config = $api->getTokens($code);
        $api->setAccessToken($config);

        /**
         * @var $user User
         */
        $user = $api->executeCommand('GetCurrentUser');
        $response = $this->oauthAuthorize($provider, $user);

        $this->addAccessToken($provider,
            array_merge($config, ['private_access_token' => $user->getPrivateToken()]));

        return $this->app->json($response, ($response['success'] == true ? Response::HTTP_OK : Response::HTTP_FORBIDDEN));
    }

    /**
     * @todo deprecated Not usable
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     *
     * @throws \Exception
     */
    public function addCredentialAction(Request $request)
    {
        $code = $request->request->get('code', null);
        $provider = $request->request->get('provider', 'gitlab');

        /**
         * @var $api Api
         */
        $api = $this->app['gitlab_api'];

        if (null === $code) {
            throw new BadRequestHttpException('Bad request params');
        }

        $config = $api->getTokens($code);
        $api->setAccessToken($config);

        /**
         * @var $user User
         */
        $user = $api->executeCommand('GetCurrentUser');

        $this->addAccessToken($provider,
            array_merge($config, ['private_access_token' => $user->getPrivateToken()]));

        return $this->app->json(['meta' => ['success' => true]]);
    }

    /**
     * Регистрация нового пользователя
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function registerAction(Request $request)
    {
        $vars = $request->request->all();

        $userProvider = $this->app['users'];

        $response = ['success' => true];

        try {
            $user = $userProvider->registerUser($vars);

            $token = $this->app['security']->getToken();
            $token->setUser($user);

            $this->addAccessToken('gitlab',
                ['private_access_token' => $vars['_token']]);
        } catch (BadRequestHttpException $e) {
            $response = [
                'success' => false,
                'error'   => 'Invalid login or email'
            ];
        }

        return $this->app->json($response, ($response['success'] == true ? Response::HTTP_OK : Response::HTTP_BAD_REQUEST));
    }

    /**
     * @todo deprecated Not usable
     *
     * Добавления токена для доступа пользователю
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function saveTokenAction(Request $request)
    {
        $token = $request->request->get('_token', null);
        $provider = $request->request->get('provider', 'gitlab');

        $this->addAccessToken($provider,
             ['private_access_token' => $token]);

        return $this->app->json('OK');
    }

    /**
     * @param int $provider
     * @param User $user
     *
     * @return array
     */
    private function oauthAuthorize($provider,  User $user)
    {
        $userName = $user->getUsername();

        /**
         * @var $userProvider UserProvider
         */
        $userProvider = $this->app['users'];

        if (! $kanbanUser = $userProvider->loadUserByOauth($provider, $user->getId())) {
            $userName .= '_'.$provider;

            $userProvider->registerUser([
                '_username' => $userName,
                '_email' => $user->getEmail(),
                '_password' => $user->getPrivateToken()
            ], ['provider_id' => $provider, 'user_id' => $user->getId()]);

            $kanbanUser = $userProvider->loadUserByUsername($userName);
        }

        $token = $this->app['security']->getToken();
        $token->setUser($kanbanUser);

        $response = [
            'success' => true,
            'token' => $this->app['security.jwt.encoder']->encode(['name' => $kanbanUser->getUsername()]),
        ];

        return $response;
    }

    /**
     * @param int $provider
     * @param array $tokens
     */
    private function addAccessToken($provider, $tokens)
    {
        $user = $this->app['security']->getToken()->getUser();

        $user->setCredential($provider, $tokens);

        $userProvider = $this->app['users'];
        $userProvider->updateCredential($user);
    }
}