<?php

namespace Kanban\Controller;

use Guzzle\Http\Exception\BadResponseException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class UserController
{

    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function indexAction(Request $request)
    {
        $project_id = $request->query->get('project_id', 0);
        $users = $this->app['gitlab_api']->executeCommand('GetProjectMembers', ['project_id'=>$project_id]);
        $result = [];

        /**
         * @var $user \Gitlab\Models\User
         */
        foreach($users as $user) {
            $result[$user->getId()] = [
                'id' => $user->getId(),
                'username' => $user->getUsername(),
                'name'  => $user->getName(),
                'avatar_url' => $user->getAvatarUrl(),
            ];
        };


        /**
         * @todo хак для получения юзеров относящихся к группе но не относящихся к проектам в группе
         */
        $project = $this->app['gitlab_api']->executeCommand('GetProject', ['project_id'=>$project_id]);
        $group_id = $project->getNamespace()['id'];

        $groupMembers = array();
        if ($group_id !== 0) {
            try {
                $groupMembers = $this->app['gitlab_api']->executeCommand('GetGroupMembers', ['group_id'=> $group_id]);
            } catch (BadResponseException $e) {
                $groupMembers = [];
            }
        }

        /**
         * @var $user \Gitlab\Models\User
         */
        foreach($groupMembers as $user) {
            $result[$user->getId()] = [
                'id' => $user->getId(),
                'username' => $user->getUsername(),
                'name'  => $user->getName(),
                'avatar_url' => $user->getAvatarUrl(),
            ];
        };

        return $this->app->json(array_values($result));
    }
}