<?php

namespace Kanban\Model;

use Symfony\Component\Security\Core\User\AdvancedUserInterface;

class User implements AdvancedUserInterface
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;
    /**
     * @var bool
     */
    private $enabled = true;
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $salt;
    /**
     * @var bool
     */
    private $accountNonExpired  = true;
    /**
     * @var bool
     */
    private $credentialsNonExpired = true;
    /**
     * @var bool
     */
    private $accountNonLocked = true;
    /**
     * @var string
     */
    private $roles;

    /**
     * @var array
     */
    private $credentials = [];

    public function __construct($fields)
    {
        foreach ($fields as $field => $value) {
            $this->{$field} = $value;
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return boolean
     */
    public function isAccountNonExpired()
    {
        return $this->accountNonExpired;
    }

    /**
     * @param boolean $accountNonExpired
     */
    public function setAccountNonExpired($accountNonExpired)
    {
        $this->accountNonExpired = $accountNonExpired;
    }

    /**
     * @return boolean
     */
    public function isAccountNonLocked()
    {
        return $this->accountNonLocked;
    }

    /**
     * @param boolean $accountNonLocked
     */
    public function setAccountNonLocked($accountNonLocked)
    {
        $this->accountNonLocked = $accountNonLocked;
    }

    /**
     * @return boolean
     */
    public function isCredentialsNonExpired()
    {
        return $this->credentialsNonExpired;
    }

    /**
     * @param boolean $credentialsNonExpired
     */
    public function setCredentialsNonExpired($credentialsNonExpired)
    {
        $this->credentialsNonExpired = $credentialsNonExpired;
    }

    /**
     * @return boolean
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param boolean $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getRoles()
    {
        if (is_string($this->roles)) {
            $this->roles = json_decode($this->roles, true);
        }

        return array($this->roles);
    }

    /**
     * @param string $roles
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;
    }

    /**
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * @param string $salt
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function setCredential($provider, $params)
    {
        $this->allCredential();
        $this->credentials[$provider] = $params;
    }

    public function getCredential($provider)
    {
        $credential = $this->allCredential();
        return array_key_exists($provider, $credential) ? $credential[$provider] : [];
    }

    /**
     * @return array
     */
    public function allCredential()
    {
        if (is_string($this->credentials)) {
            $this->credentials = json_decode($this->credentials, true);
        }

        return (array) $this->credentials;
    }

    /**
     * {@inheritDoc}
     */
    public function eraseCredentials()
    {
    }
}
