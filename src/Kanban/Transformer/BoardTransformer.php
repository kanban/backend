<?php

namespace Kanban\Transformer;

use League\Fractal\TransformerAbstract;
use Gitlab\Models\Project;

class BoardTransformer extends TransformerAbstract 
{
    public function transform(Project $board) 
    {
        $result = [
            'id'                  => $board->getId(),
            'name_with_namespace' => $board->getNameWithNamespace(),
            'path_with_namespace' => $board->getPathWithNamespace(),
            'namespace'           => $board->getNamespace(),
            'name'                => $board->getName(),
            'description'         => $board->getDescription(),
            'last_modified'       => $board->getLastActivityAt()->getTimestamp(),
            'created_at'          => $board->getCreatedAt()->getTimestamp(),
            'owner'               => $board->getOwner(),
            'avatar_url'          => $board->getAvatarUrl(),
        ];

        return $result;
    }
}
